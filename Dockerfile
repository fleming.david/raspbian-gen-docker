FROM debian:jessie

MAINTAINER David B Fleming <fleming.david.b@gmail.com>

ENV DEBIAN_FRONTEND noninteractive

# Setup build environment
RUN apt-get update -qq && \
apt-get install -qqy binfmt-support quilt kpartx realpath qemu-user-static debootstrap zerofree rsync dosfstools zip && \
apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# Handle mounting /proc/sys/fs/binfmt_misc
ADD entrypoint.sh /entrypoint.sh

CMD /bin/bash -C '/entrypoint.sh'; '/bin/bash' 
